# Pokebot

A solution for the **"Pokebot"** assignment in the CPP Embedded Development Bootcamp. The program pulls a random pokemon through the **PokeApi**, and puts it to a database in the case, that the pokemon hasn't been already added.

# Requirements

This program requires `cmake`, `python3`, `pip3`, `gcc`, `restclient-cpp`, `nlohmann-json`

# Usage

Figure out how to build the program. Get the submodules and start the database:

    $ git submodule init
    $ git submodule update
    $ ./runserver.sh

If you cannot run the bash-script use the following command:

    $ chmod +x runserver.sh

If you have installed all dependecies you can do the cmake. Replace <YOUR_PROJECT_ROOT_DIRECTORY> and <YOUR_PROJECT_BUILD_DIRECTORY> with correct entry points eg. /home/your_user_name/your_project_folder/pokebot and /home/your_user_name/your_project_folder/pokebot/build and run the cmake and make

    $ /usr/local/bin/cmake --no-warn-unused-cli -DCMAKE_TOOLCHAIN_FILE:STRING=/opt/vcpkg/scripts/buildsystems/vcpkg.cmake -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/gcc-11 -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++-11 -H<YOUR_PROJECT_ROOT_DIRECTORY> -B<YOUR_PROJECT_BUILD_DIRECTORY> -G Ninja

If you have installed all dependecies you can do the make. Replace <YOUR_PROJECT_ROOT_DIRECTORY> and <YOUR_PROJECT_BUILD_DIRECTORY> with correct entry points eg. /home/your_user_name/your_project_folder/pokebot and /home/your_user_name/your_project_folder/pokebot/build 

    $ /usr/local/bin/cmake --build <YOUR_PROJECT_BUILD_DIRECTORY> --config Debug --target all -j 6 --

Run the program:

    $ ./build/Pokebot

Fetch your pokemons:

    $ curl localhost:5000/fetch/all

Good luck!

# Contributors

[**@raiwal (Rainer Waltzer)**](https://gitlab.com/raiwal)

[**@rootElmo (Elmo Rohula)**](https://gitlab.com/rootElmo)

# License
