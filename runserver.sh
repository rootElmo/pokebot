#!/bin/bash

run_flag=1

echo "Running GDBR server..."

if [ ! command -v python3 &> /dev/null ]; then
    echo "Python3 is not installed"
    run_flag=0
fi

if [ ! command -v pip3 &> /dev/null ]; then
    echo "pip3 is not installed"
    run_flag=0
fi

if [ ! run_flag ]; then
    echo "Please install required programs"
    exit 1
fi


if [ ! -f ./python-generic-db-rest-server/src/main.py ]; then
    echo "GDBR server files not found"
    printf "Try the following commands:\ngit submodule init\ngit submodule update\n"
    exit 1
fi

cd python-generic-db-rest-server
pip3 install -r requirements.txt
python3 ./src/main.py
