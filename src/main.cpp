#include <iostream>
#include <random>
#include <functional>
#include <string>
#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"
#include <nlohmann/json.hpp>
#include <vector>

// Random generation setup
std::random_device rd; // this is required to get different results
std::uniform_int_distribution<int> distribution(1,898); 
std::default_random_engine generator{rd()}; // and parametrization
auto random_pokemon_id = bind(distribution, generator); // from functional

std::string base_url = "https://pokeapi.co/api/v2/pokemon/"; // Pokemon API base URL

bool new_pokemon = true;

std::vector<std::string> data; // container for fetched database data

// Database JSON identifiers
std::string db_meta = "meta";
std::string db_data = "data";
std::string meta_name = "pokemon";

// Add Pokemon data to database
bool add_pokemon(std::string body)
{
    RestClient::init();
    RestClient::Connection* conn = new RestClient::Connection("http://127.0.0.1:5000");
    conn->AppendHeader("Content-Type","application/json");

    auto json_body = nlohmann::json::parse(body);

    nlohmann::json j;
    nlohmann::json pokemon_id;
    nlohmann::json pokemon_sprites;
    nlohmann::json pokemon_data_fields;

    pokemon_id = json_body["id"];
    pokemon_sprites = json_body["sprites"]["front_default"];

    j[db_meta] = meta_name;
    for (int i=0; i<json_body["types"].size(); i++) {
        j["type"][i] = json_body["types"][i]["type"]["name"];
    }    

    nlohmann::json id_object;
    id_object.emplace("id", pokemon_id);
    id_object.emplace("type", j["type"]);
    id_object.emplace("sprite", pokemon_sprites);
    pokemon_data_fields = { id_object };
    j[db_data] = pokemon_data_fields.dump();

    RestClient::Response r = conn->post("/submit",j.dump());
    RestClient::disable();

    if (r.code == 201) // success
    {
        return true;
    }
    return false;
}

// We clear the container if we got the the response. 
bool get_data_from_db()
{
    RestClient::Response r{};
    try
    {
        r = RestClient::get("http://127.0.0.1:5000/fetch/all");
    }
    catch (...) 
    {
        std::cout << "Could not connect to the database server!" << std::endl;
        return false;
    }
    
    if(r.code == 200) // success
    {
        data.clear(); 
        // Strong assumption that database returns
        // data in JSON format
        auto json_body = nlohmann::json::parse(r.body);
        for (auto& [key, value] : json_body.items())
        {
            data.push_back(value["data"]);
        }
        return true;
    }
    else
    {
        std::cout << "Error: Could not fetch data from database!" << std::endl;
        return false;
    }
    return false;
}

bool check_id_from_db(int random_id)
{
    bool found = false;
    nlohmann::json row_object;
    for (int i = 0; i < data.size(); i++)
    {
        auto row_data = nlohmann::json::parse(data.at(i));
        if (row_data[0]["id"] == random_id)
        {
            found = true;
        }        
    }
    return found;
}

int main() {
    std::cout << "Starting Pokebot\n\n";
    std::cout << "Fetch new Pokemons and adds unknown Pokemons to the database" << std::endl;

    while(new_pokemon)
    {
        // Create random endpoint for new Pokemon
        int random_id = random_pokemon_id();
        std::string random_pokemon_url = base_url + std::to_string(random_id);
        // Get random Pokemon
        RestClient::Response r = RestClient::get(random_pokemon_url);
        if (r.code == 200) // request OK
        {
            new_pokemon = false;
            // try to update vector data from database
            if(!get_data_from_db())
            {
                return -1; // database connection error 
            } 
            
            if (!check_id_from_db(random_id))
            {
                std::cout << "New Pokemon found! Adding data... ";
                new_pokemon = true;
                if (add_pokemon(r.body))
                {
                    std::cout << "Added!" << std::endl;
                }
                else
                {
                    std::cout << "Adding failed!" << std::endl;
                }               
            }
            else
            {
                std::cout << "Pokemon exists! Quit." << std::endl;
                new_pokemon = false;
            }
        }
        else
        {
            std::cout << "An error has occured!" << std::endl;
            new_pokemon = false;
        }
    }    
}

